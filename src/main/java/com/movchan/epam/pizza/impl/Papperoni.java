package com.movchan.epam.pizza.impl;

import com.movchan.epam.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Papperoni implements Pizza {
    private static final Logger LOGGER = LogManager.getLogger(Papperoni.class);
    private int price = 95;
    private int weight = 400;
    @Override
    public void bake() {
        LOGGER.info("Prepare Papperoni pizza");
    }

    @Override
    public void cut() {
        LOGGER.info("Cut Papperoni pizza");
    }

    @Override
    public void box() {
        LOGGER.info("Box Papperoni pizza");
    }

    @Override
    public void order() {
        LOGGER.info("Order Papperoni pizza");
    }

    @Override
    public void description() {
        LOGGER.info("Description Papperoni pizza");
    }


    public int getPrice() {
        return price;
    }

    @Override
    public int getWeight() {
        return weight;
    }
}
