package com.movchan.epam.city;

public interface City {
    int price(int price);
    int weight(int weight);
    String creatTradition();
}
