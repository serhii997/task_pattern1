package com.movchan.epam.city.impl;

import com.movchan.epam.city.City;

public class Kyiv implements City {
    double procentPrice = 1.5;
    double procentWeight = 0.9;

    @Override
    public int price(int pizzaPrice) {
        return (int) Math.round(pizzaPrice * procentPrice);
    }

    @Override
    public int weight(int pizzaWeight) {
        return (int) Math.floor(pizzaWeight * procentWeight);
    }

    @Override
    public String creatTradition() {
        return "free drink";
    }
}