package com.movchan.epam.pizza.impl;

import com.movchan.epam.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Clam implements Pizza {
    private static final Logger LOGGER = LogManager.getLogger(Clam.class);
    private int price = 100;
    private int weight = 450;
    @Override
    public void bake() {
        LOGGER.info("Prepare Clam pizza");
    }

    @Override
    public void cut() {
        LOGGER.info("Cut Clam pizza");
    }

    @Override
    public void box() {
        LOGGER.info("Box Clam pizza");
    }

    @Override
    public void order() {
        LOGGER.info("Order Clam pizza");
    }

    @Override
    public void description() {
        LOGGER.info("Description Clam pizza");
    }

    public int getPrice() {
        return price;
    }

    @Override
    public int getWeight() {
        return weight;
    }

}
