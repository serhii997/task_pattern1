package com.movchan.epam.factory;

import com.movchan.epam.AllCity;
import com.movchan.epam.TypePizza;
import com.movchan.epam.city.City;
import com.movchan.epam.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class Ristorante {
    private static final Logger LOGGER = LogManager.getLogger(Ristorante.class);

    protected abstract Pizza createPizza(TypePizza typePizza);

    public Pizza assemble(TypePizza typePizza) {
        Pizza pizza = createPizza(typePizza);
        pizza.description();
        pizza.bake();
        pizza.box();
        pizza.cut();
        pizza.order();
        return pizza;
    }

    protected abstract City getCity(AllCity allCity);

    public City informationCity(AllCity allCity, int price, int weight) {
        City city = getCity(allCity);
        LOGGER.info(city.price(price) + " UAH");
        LOGGER.info(city.weight(weight) + " gram");
        LOGGER.info(city.creatTradition());
        return city;
    }
}
