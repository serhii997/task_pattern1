package com.movchan;

import com.movchan.epam.AllCity;
import com.movchan.epam.TypePizza;
import com.movchan.epam.city.City;
import com.movchan.epam.factory.Ristorante;
import com.movchan.epam.factory.impl.AllRistorante;
import com.movchan.epam.pizza.Pizza;

public class Application {
    public static void main(String[] args) {
        Ristorante ristorante = new AllRistorante();
        Pizza pizza = ristorante.assemble(TypePizza.PALERMO);

        City city = ristorante.informationCity(AllCity.LVIV, pizza.getPrice(),pizza.getWeight());


    }
}
