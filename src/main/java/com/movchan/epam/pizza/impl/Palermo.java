package com.movchan.epam.pizza.impl;

import com.movchan.epam.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Palermo implements Pizza {
    private static final Logger LOGGER = LogManager.getLogger(Palermo.class);
    private int price = 130;
    private int weight = 390;
    @Override
    public void bake() {
        LOGGER.info("Prepare Palermo pizza");
    }

    @Override
    public void cut() {
        LOGGER.info("Cut Palermo pizza");
    }

    @Override
    public void box() {
        LOGGER.info("Box Palermo pizza");
    }

    @Override
    public void order() {
        LOGGER.info("Order Palermo pizza");
    }

    @Override
    public void description() {
        LOGGER.info("Description Palermo pizza");
    }


    public int getPrice() {
        return price;
    }

    @Override
    public int getWeight() {
        return weight;
    }
}
