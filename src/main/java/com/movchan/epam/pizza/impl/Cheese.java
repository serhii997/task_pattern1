package com.movchan.epam.pizza.impl;

import com.movchan.epam.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Cheese implements Pizza {

    private final static Logger LOGGER = LogManager.getLogger(Cheese.class);
    private int price = 90;
    private int weight = 430;
    @Override
    public void bake() {
        LOGGER.info("Prepare Cheese pizza");
    }

    @Override
    public void cut() {
        LOGGER.info("Cut Cheese pizza");
    }

    @Override
    public void box() {
        LOGGER.info("Box Cheese pizza");
    }

    @Override
    public void order() {
        LOGGER.info("Order Cheese pizza");
    }

    @Override
    public void description() {
        LOGGER.info("Description Cheese pizza");
    }


    public int getPrice() {
        return price;
    }

    @Override
    public int getWeight() {
        return weight;
    }
}
