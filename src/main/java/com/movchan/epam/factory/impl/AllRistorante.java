package com.movchan.epam.factory.impl;

import com.movchan.epam.AllCity;
import com.movchan.epam.TypePizza;
import com.movchan.epam.city.City;
import com.movchan.epam.city.impl.Kharkiv;
import com.movchan.epam.city.impl.Kyiv;
import com.movchan.epam.city.impl.Lviv;
import com.movchan.epam.factory.Ristorante;
import com.movchan.epam.pizza.Pizza;
import com.movchan.epam.pizza.impl.*;

public class AllRistorante extends Ristorante {
    @Override
    protected Pizza createPizza(TypePizza typePizza) {
        Pizza pizza = null;

        if(typePizza == TypePizza.CHEESE){
            pizza = new Cheese();
        } else if (typePizza == TypePizza.CLAM) {
            pizza = new Clam();
        } else if (typePizza == TypePizza.PALERMO) {
            pizza = new Palermo();
        } else if (typePizza == TypePizza.PAPPERONI) {
            pizza = new Papperoni();
        } else if (typePizza == TypePizza.VEGGIE) {
            pizza = new Veggie();
        }

        return pizza;
    }

    @Override
    protected City getCity(AllCity allCity) {
        City city = new Lviv();

        if(allCity == AllCity.LVIV){
            city = new Lviv();
        } else if (allCity == AllCity.KYIV) {
            city = new Kyiv();
        } else if (allCity == AllCity.KHARKIV) {
            city = new Kharkiv();
        }

        return city;
    }

}
