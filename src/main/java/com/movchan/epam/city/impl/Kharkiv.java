package com.movchan.epam.city.impl;

import com.movchan.epam.city.City;

public class Kharkiv implements City {


    final double procentPrice = 1;
    final double procentWeight = 1;

    @Override
    public int price(int pizzaPrice) {
        return (int) Math.round(pizzaPrice * procentPrice);
    }

    @Override
    public int weight(int pizzaWeight) {
        return (int) Math.floor(pizzaWeight * procentWeight);
    }

    @Override
    public String creatTradition() {
        return "free delivery";
    }
}
