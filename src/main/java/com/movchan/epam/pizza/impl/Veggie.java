package com.movchan.epam.pizza.impl;

import com.movchan.epam.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Veggie implements Pizza {
    private static final Logger LOGGER = LogManager.getLogger(Veggie.class);
    private int price = 130;
    private int weight = 400;
    @Override
    public void bake() {
        LOGGER.info("Prepare Veggie pizza");
    }

    @Override
    public void cut() {
        LOGGER.info("Cut Veggie pizza");
    }

    @Override
    public void box() {
        LOGGER.info("Box Veggie pizza");
    }

    @Override
    public void order() {
        LOGGER.info("Order Veggie pizza");
    }

    @Override
    public void description() {
        LOGGER.info("Description Veggie pizza");
    }


    public int getPrice() {
        return price;
    }

    @Override
    public int getWeight() {
        return weight;
    }
}
