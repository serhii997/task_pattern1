package com.movchan.epam.pizza;

public interface Pizza {
    void bake();
    void cut();
    void box();
    void order();
    void description();
    int getPrice();
    int getWeight();
}
